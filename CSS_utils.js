function id$(n) {
	return document.getElementById(n);
}
function $(n) {
	return document.querySelector(n);
}
function $$(n) {
	return document.querySelectorAll(n);
}

function CSSsize(value, unit) {
	this.value = value;
	this.unit = unit;
}
CSSsize.prototype.toString = function() {
	return format("{0} {1}", this.value, this.unit);
}
CSSsize.prototype.valueOf = function() {
	return this.toString();//TODO: make it return pixel amount
}

function maxScrollTop(element) {
	return element.scrollHeight - element.clientHeight;
}

function smoothScrollTo(target) {
	const
		MIN_PIXELS_PER_STEP = 16,
		MAX_SCROLL_STEPS = 30;

	var scrollContainer = target;

	/* Find scroll container */
	do {
		scrollContainer = scrollContainer.parentNode;
		//if no parent can be scrolled - end
		if(!scrollContainer)
			return;
		scrollContainer.scrollTop += 1;
	} while(scrollContainer.scrollTop == 0);// repeat if can't be scrolled


	var targetY = 0;
	/* Find the top of target relatively to the container */
	do {
		if(target == scrollContainer)
			break;
		targetY += target.offsetTop;
	} while(target = target.offsetParent);

	var pixelsPerStep = Math.max(
		MIN_PIXELS_PER_STEP,
		(targetY - scrollContainer.scrollTop) / MAX_SCROLL_STEPS
	);
	/* Scroll */
	function stepFunc() {
		scrollContainer.scrollTop = Math.min(
			targetY,
			pixelsPerStep+scrollContainer.scrollTop
		);
		if(scrollContainer.scrollTop >= targetY || scrollContainer.scrollTop == maxScrollTop(scrollContainer))
			return;
		window.requestAnimationFrame(stepFunc);
	};
	window.requestAnimationFrame(stepFunc);

}

