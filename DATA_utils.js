/* BOOL */
function XOR(a, b) {
	return a ? !b : b;
}
function XNOR(a, b) {
	return a ? b : !b;
}

/* INT */
function random(min, max) {
	var ran = Math.floor(Math.random() * (max - min + 1)) + min;
	return ran;
}


/* STRING */
String.prototype.format = String.prototype.f = function() {
	var s = this, i = arguments.length;
	while (i--)
		s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i]);
	return s;
};
function format() {
	var s = arguments[0], i = arguments.length;
	while(i--)
		s = s.replace(new RegExp('\\{' + (i-1) + '\\}', 'gm'), arguments[i]);
	return s;
}

/* ARRAY */
Array.prototype.remove = function(el) {
	var index = this.indexOf(el);
	if(index >= 0)
		arr.splice(index, 1 );
	return index;
}
function remove(arr, el) {
	var index = arr.indexOf(el);
	if(index >= 0)
		arr.splice(index, 1 );
	return index;
};

function shuffle(arr) {
	for(var i = arr.length-1; i>0; --i) {

		var
			j = Math.floor(Math.random()*(i+1)),
			temp = arr[i];

		arr[i] = arr[j];
		arr[j] = temp;
	}
}

/* 2D ARRAY */
function initMatrix(rows, cols, val = 0) {

	var m = []

	for(var i = 0; i<rows; ++i) {
		m[i] = [];
		for(var j = 0; j<cols; ++j)
			m[i][j] = val;
	}

	return m;
}

/* DOM */
function removeAllChildren(element) {
	while(element.hasChildNodes())
		element.removeChild(element.lastChild);
}

/* LOCAL STORAGE */
function localStorageTest() { 
	var test = 'test';
	try {
		localStorage.setItem(test, test);
		localStorage.removeItem(test);
		return true;
	} catch(e) {
		return false;
	}
}

/* DOWNLOAD FILES */
function downloadFile(content, fileName="download.data", contentType="application/octet-stream") {

	var
		temp_a = document.createElement("a"),
		blob = new Blob([content], {"type": contentType});

	//temp_a.href = 'data:text/plain;charset=utf-8,' + encodeURIComponent(text)); // can be used instead of Blob, but has size limit
	temp_a.href = window.URL.createObjectURL(blob);
	temp_a.download = fileName;

	if(document.createEvent) {
		var event = document.createEvent('MouseEvents');
		event.initEvent('click', true, true);
		temp_a.dispatchEvent(event);
	}
	else
	temp_a.click();
}

