const
	DEFAULT_SAVE_NAME = "save",
	AUTO_SAVE_NAME = "autosave",
	SAVE_NAME_MASK = /^[a-zA-Z][a-zA-Z0-9]*$/;

/* INITIALIZATION */
function initBoard(rows, cols, moves, width, height, config={autoSave: false, showSolution: false}) { // INITIALIZATION
	var board = {
	// BASIC GAME PROPERTIES
		rows: rows,
		cols: cols,
		moves: Math.min(moves, cols*rows),

	// GAME SIZE
		width: width,
		height: height,

	// CONFIGURATION
		config: config
	};

	board.div = construct(board);
	board.stepCounter = document.createElement("span");

	return board;
}

/* LOGIC PART */
function fieldUpdate(board, i, j) { // ON MOVE BOARD STATE UPDATE

	board.state[i][j] = !board.state[i][j]; // middle
	if(i>0)
		board.state[i-1][j] = !board.state[i-1][j]; // upper
	if(i<board.rows-1)
		board.state[i+1][j] = !board.state[i+1][j]; // bottom
	if(j>0)
		board.state[i][j-1] = !board.state[i][j-1]; // left
	if(j<board.cols-1)
		board.state[i][j+1] = !board.state[i][j+1]; // right
}

/* DISPLAY PART */
function construct(board) { // DIV STRUCTURE CREATION

	const DIMSTRFORMAT = "{0}{1}";

	var rootDiv = document.createElement("div");
	rootDiv.id = "board";
	rootDiv.style.width  = DIMSTRFORMAT.format(board.width.value, board.width.unit);
	rootDiv.style.height = DIMSTRFORMAT.format(board.height.value, board.height.unit);

	const
		FIELD_WIDTH  = DIMSTRFORMAT.format(board.width.value / board.cols - 10, board.width.unit),
		FIELD_HEIGHT = DIMSTRFORMAT.format(board.height.value / board.rows - 10, board.height.unit);

	for(var i = 0; i < board.rows; ++i)
		for(var j = 0; j < board.cols; ++j) {
			var field = document.createElement("div");

			field.classList.add("field");
			field.id = "field{0}_{1}".format(i, j);
			field.style.width = FIELD_WIDTH;
			field.style.height = FIELD_HEIGHT;
			field.onclick = function() {
				onFieldClick(board, this.id);
			};

			rootDiv.appendChild(field);
		}

	return rootDiv;
}
function update(board) { // DISPLAY (DIV CLASSES) UPDATE

	for(var i = 0; i < board.rows; ++i)
		for(var j = 0; j < board.cols; ++j) {

			var tempDiv = board.div.children[i*board.cols + j];

			if(board.state[i][j])
				tempDiv.classList.add("active");
			else
				tempDiv.classList.remove("active");

			if(board.config.showSolution && board.solution[i][j])
				tempDiv.classList.add("solution");
			else
				tempDiv.classList.remove("solution");
		}
}


/* CONTROL PART */
function onFieldClick(board, idStr) { // CLICK HANDLING
	// get field indexes (from field id)
	var index = 5;
	var i = "";
	var j = "";
	while(idStr.charAt(index) != '_')
		i += idStr.charAt(index++);
	index += 1;
	while(index < idStr.length)
		j += idStr.charAt(index++);
	i = parseInt(i, 10);
	j = parseInt(j, 10);

	// solution update
	board.solution[i][j] = !board.solution[i][j];

	// save player move
	board.pmoves.push([i, j]);

	// field state
	fieldUpdate(board, i, j);

	// update field color (class)
	update(board);

	// update step counter
	board.stepCounter.innerHTML = board.pmoves.length.toString();

	// update win counter and check if riddle solved
	board.moves2win += board.solution[i][j] ? 1 : -1;
	if(board.moves2win === 0)
		endGame(board);

	// autosave
	if(board.config.autoSave)
		saveGame(
			board,
			board.saveName === undefined ? AUTO_SAVE_NAME : board.saveName
		);
}

/* INTERFACE */
function newGame(rows, cols, moves, width, height, config=undefined) {

	var board = initBoard(rows, cols, moves, width, height, config);

	board.moves2win = board.moves; // moves required to win
	board.cmoves = []; // computer generated steps (each must be unique)
	board.pmoves = []; // player move history
	board.state = initMatrix(board.rows, board.cols, false); // field states
	board.solution = initMatrix(board.rows, board.cols, false); // fields which when clicked will solve the riddle

	// randomize the riddle
	var temp = [];
	for(var i = 0; i<board.rows; ++i)
		for(var j = 0; j<board.cols; ++j)
			temp.push([i, j]);
	shuffle(temp);
	board.cmoves = temp.slice(0, board.moves);
	for(var arr of board.cmoves) {
		fieldUpdate(board, arr[0], arr[1]);
		board.solution[arr[0]][arr[1]] = true;
	}

	update(board);

	board.stepCounter.innerHTML = "0";

	return board;
}

function saveGame(board, saveName=DEFAULT_SAVE_NAME) {

	var data = {
		rows: board.rows,
		cols: board.cols,
		moves: board.moves,
		moves2win: board.moves2win,// this could be ommitted and deduced from board.solution
		config: board.config,
		cmoves: board.cmoves,
		pmoves: board.pmoves
	};

	if(SAVE_NAME_MASK.test(data.saveGame))
		localStorage[saveName] = JSON.stringify(data);
	else
		throw {
			what: "saveGameFailure",
			saveName: saveName,
			message: "saveName must consist of letters only."
		};
}
function loadGame(width, height, saveName) {

	var data = JSON.parse(localStorage[saveName]);
	var board = initBoard(data.rows, data.cols, data.moves, width, height, data.config);

	board.moves2win = data.moves2win;
	board.cmoves = data.cmoves;
	board.pmoves = data.pmoves;
	board.state = initMatrix(board.rows, board.cols, false);
	board.solution = initMatrix(board.rows, board.cols, false);

	function helper(a) {
		for(var arr of a) {
			fieldUpdate(board, arr[0], arr[1]);
			board.solution[arr[0]][arr[1]] = !board.solution[arr[0]][arr[1]];
		}
	}
	helper(board.cmoves);
	helper(board.pmoves);

	update(board);

	board.stepCounter.innerHTML = board.pmoves.length.toString();

	return board;
}

function endGame(board) {

	const MESSAGE = "Congratuations!\nRiddle solved with {0} steps!\n(minimum steps required: {1})."

	alert(format(
		MESSAGE,
		board.stepCounter.innerHTML,
		board.moves
	));

	board.div.style.display = "none";
	board.stepCounter.parentNode.style.display = "none";
}

